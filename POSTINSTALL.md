<sso>

By default, Cloudron users cannot login until `is_staff` flag is set.

First, try to login, it creates an appropriate account in the ArchiveBox database, then
to set `is_staff` you should run the following commands in Console:

Don't forget to replace `YOUR_USERNAME` with your Cloudron username.

```
$ cd /app/data/archivebox/
$ gosu cloudron:cloudron archivebox manage shell <<EOF
from django.contrib.auth.models import User
User.objects.filter(username='YOUR_USERNAME').update(is_staff=True)
EOF
```

To make an user as `superadmin`, you should run:
```
$ cd /app/data/archivebox/
$ gosu cloudron:cloudron archivebox manage shell <<EOF
from django.contrib.auth.models import User
User.objects.filter(username='YOUR_USERNAME').update(is_superuser=True, is_staff=True)
EOF
```
Restart the app for the changes to become active.

</sso>

<nosso>
This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password**: changeme123<br/>

Please change the admin password immediately!
</nosso>
